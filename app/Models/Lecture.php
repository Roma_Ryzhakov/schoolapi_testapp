<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    use HasFactory;

    protected $fillable = ['theme', 'description'];

    public function classRoom()
    {
        return $this->belongsToMany(ClassRoom::class, 'curriculum');
    }

    public function students()
    {
        return $this->belongsToMany(Student::class, 'student_lecture');
    }

    public function curriculum()
    {
        return $this->hasMany(Curriculum::class);
    }
}
