<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Curriculum extends Model
{
    use HasFactory;

    protected $fillable = ['class_room_id', 'lecture_id', 'order'];
    protected $table = 'curriculum';

    public function classRoom()
    {
        return $this->belongsTo(ClassRoom::class);
    }

    public function lecture()
    {
        return $this->belongsTo(Lecture::class);
    }

}
