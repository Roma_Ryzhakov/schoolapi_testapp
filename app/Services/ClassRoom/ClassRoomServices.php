<?php

namespace App\Services\ClassRoom;

use App\Http\Resources\Curriculum\CurriculumResource;
use App\Models\ClassRoom;
use App\Models\Curriculum;

class ClassRoomServices
{
    public function getAllClassRooms()
    {
        return ClassRoom::all();
    }

    public function getClassRoom($id)
    {
        return ClassRoom::with('students')->findOrFail($id);
    }

    public function storeClassRoom($request)
    {
        return ClassRoom::create($request->validated());
    }

    public function updateClassRoom($request, $id)
    {
        $classRoom = ClassRoom::findOrFail($id);
        $classRoom->update($request->validated());
        return $classRoom;
    }

    public function deleteClassRoom($id)
    {
        $classRoom = ClassRoom::findOrFail($id);
        $classRoom->delete();
        return null;
    }
}
