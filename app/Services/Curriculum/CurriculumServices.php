<?php

namespace App\Services\Curriculum;

use App\Http\Requests\Curriculum\UpdateCurriculumRequest;
use App\Http\Resources\Curriculum\CurriculumResource;
use App\Models\ClassRoom;
use App\Models\Curriculum;

class CurriculumServices
{
    public function getCurriculum($classRoom) {

        $classRoom = ClassRoom::findOrFail($classRoom->id);
        return $classRoom->curriculum()->with('lecture')->orderBy('order')->get();

    }

    public function updateCurriculum($request, $id)
    {
        $classroom = Classroom::findOrFail($id);

        Curriculum::where('class_room_id', $id)->delete();

        foreach ($request->validated()['curriculum'] as $item) {
            Curriculum::create([
                'class_room_id' => $id,
                'lecture_id' => $item['lecture_id'],
                'order' => $item['order'],
            ]);
        }

        return $classroom->curriculum()->with('lecture')->orderBy('order')->get();
    }
}
