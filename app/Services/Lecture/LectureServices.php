<?php

namespace App\Services\Lecture;

use App\Http\Resources\Lecture\LectureResource;
use App\Models\Lecture;

class LectureServices
{
    public function getAllLectures()
    {
        return Lecture::all();
    }

    public function getLecture($id)
    {
        return Lecture::with('classRoom', 'students')->findOrFail($id);
    }

    public function storeLecture($request)
    {
        return Lecture::create($request);
    }

    public function updateLecture($request, $id)
    {
        $lecture = Lecture::findOrFail($id);
        $lecture->update($request->validated());
        return $lecture;
    }

    public function deleteLecture($id)
    {
        $lecture = Lecture::findOrFail($id);
        $lecture->delete();
        return null;
    }
}
