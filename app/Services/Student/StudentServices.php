<?php

namespace App\Services\Student;

use App\Http\Resources\Student\StudentResource;
use App\Models\Student;

class StudentServices
{
    public function getAllStudents()
    {
        return Student::all();
    }

    public function getStudent($student)
    {
        return Student::with(['classRoom', 'lectures'])->findOrFail($student->id);
    }

    public function storeStudent($request)
    {
        return Student::create($request);
    }

    public function updateStudent($request, $id)
    {
        $student = Student::findOrFail($id);
        $student -> update($request->validated());
        return $student;
    }

    public function deleteStudent($id)
    {
        $student = Student::findOrFail($id);
        $student->delete();
        return null;
    }
}
