<?php

namespace App\Http\Resources\Lecture;

use App\Http\Resources\ClassRoom\ClassRoomResource;
use App\Http\Resources\Student\StudentResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LectureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'theme' => $this->theme,
            'description' => $this->description,
            'classRooms' => ClassRoomResource::collection($this->whenLoaded('classRooms')),
            'students' => StudentResource::collection($this->whenLoaded('students')),
        ];
    }
}
