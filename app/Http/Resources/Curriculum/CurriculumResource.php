<?php

namespace App\Http\Resources\Curriculum;

use App\Http\Resources\ClassRoom\ClassRoomResource;
use App\Http\Resources\Lecture\LectureResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CurriculumResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'class_room_id' => $this->classRoom->name,
            'lecture_id' => new LectureResource($this->lecture),
            'order' => $this->order,
        ];
    }
}
