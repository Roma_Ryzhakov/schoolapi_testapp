<?php

namespace App\Http\Resources\Student;

use App\Http\Resources\ClassRoom\ClassRoomResource;
use App\Http\Resources\Lecture\LectureResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class StudentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'classRoom' => new ClassRoomResource($this->whenLoaded('classRoom')),
            'lectures' => LectureResource::collection($this->whenLoaded('lectures')),
        ];
    }
}
