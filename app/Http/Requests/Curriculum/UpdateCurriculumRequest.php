<?php

namespace App\Http\Requests\Curriculum;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCurriculumRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'curriculum' => 'required|array',
            'curriculum.*.lecture_id' => 'required|exists:lectures,id',
            'curriculum.*.order' => 'required|integer',
        ];
    }
    public function messages(): array
    {
        return [
            'curriculum.required' => 'Поле обязательно для заполнения',
        ];
    }
}
