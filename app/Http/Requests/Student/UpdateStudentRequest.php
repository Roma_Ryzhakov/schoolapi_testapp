<?php

namespace App\Http\Requests\Student;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:students',
            'class_room_id' => 'nullable|exists:class_rooms,id',
        ];
    }
    public function messages(): array
    {
        return [
            'name.required' => 'Поле имя обязательно к заполнению.',
            'email.required' => 'Поле почта обязательно к заполнению.',
            'email.email' => 'Введите не почта.',
            'email.unique' => 'Такая почта уже существует.',
            'class_room_id.exists' => 'Такого класса не существует.',
        ];
    }
}
