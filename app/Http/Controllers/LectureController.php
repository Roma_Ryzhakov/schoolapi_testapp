<?php

namespace App\Http\Controllers;

use App\Http\Requests\Lecture\StoreLectureRequest;
use App\Http\Requests\Lecture\UpdateLectureRequest;
use App\Http\Resources\Lecture\LectureResource;
use App\Models\Lecture;
use App\Services\Lecture\LectureServices;
use Illuminate\Http\Request;

class LectureController extends Controller
{
    protected LectureServices $lectureServices;
    public function __construct(LectureServices $lectureServices)
    {
        $this->lectureServices = $lectureServices;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return response()->json(LectureResource::collection($this->lectureServices->getAllLectures()));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreLectureRequest $request)
    {
        return response()->json(new LectureResource($this->lectureServices->storeLecture($request->validated())));
    }

    /**
     * Display the specified resource.
     */
    public function show(Lecture $lecture)
    {
        return response()->json(new LectureResource($this->lectureServices->getLecture($lecture->id)));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Lecture $lecture)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateLectureRequest $request, Lecture $lecture)
    {
        return response()->json(new LectureResource($this->lectureServices->updateLecture($request, $lecture->id)));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Lecture $lecture)
    {
        $this->lectureServices->deleteLecture($lecture->id);
        return response()->json(null, 204);
    }
}
