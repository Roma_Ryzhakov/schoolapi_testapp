<?php

namespace App\Http\Controllers;

use App\Http\Requests\Curriculum\UpdateCurriculumRequest;
use App\Http\Resources\Curriculum\CurriculumResource;
use App\Models\ClassRoom;
use App\Services\Curriculum\CurriculumServices;

class CurriculumController extends Controller
{
    protected CurriculumServices $curriculumServices;

    public function __construct(CurriculumServices $curriculumServices)
    {
        $this->curriculumServices = $curriculumServices;
    }

    public function getCurriculum(ClassRoom $classRoom)
    {
        return response()->json(CurriculumResource::collection($this->curriculumServices->getCurriculum($classRoom)));
    }

    public function updateCurriculum(UpdateCurriculumRequest $request, ClassRoom $classRoom)
    {
        return response()->json(CurriculumResource::collection($this->curriculumServices->updateCurriculum($request, $classRoom->id)));
    }
}
