<?php

namespace App\Http\Controllers;

use App\Http\Requests\Student\StoreStudentRequest;
use App\Http\Requests\Student\UpdateStudentRequest;
use App\Http\Resources\Student\StudentResource;
use App\Models\Student;
use App\Services\Student\StudentServices;
use Illuminate\Http\JsonResponse;

class StudentController extends Controller
{
    protected StudentServices $studentServices;

    public function __construct(StudentServices $studentService)
    {
        $this->studentServices = $studentService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return response()->json(StudentResource::collection($this->studentServices->getAllStudents()));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreStudentRequest $request)
    {
        return response()->json(new StudentResource($this->studentServices->storeStudent($request->validated())));
    }

    /**
     * Display the specified resource.
     */
    public function show(Student $student)
    {
        return response()->json(new StudentResource($this->studentServices->getStudent($student)));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateStudentRequest $request, Student $student)
    {
        return response()->json(new StudentResource($this->studentServices->updateStudent($request, $student->id)));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Student $student)
    {
        $this->studentServices->deleteStudent($student->id);
        return response()->json(null, 204);
    }
}
