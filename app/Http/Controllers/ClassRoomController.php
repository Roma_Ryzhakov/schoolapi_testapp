<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClassRoom\StoreClassRoomRequest;
use App\Http\Requests\ClassRoom\UpdateClassRoomRequest;
use App\Http\Resources\ClassRoom\ClassRoomResource;
use App\Http\Resources\Lecture\LectureCollection;
use App\Models\ClassRoom;
use App\Services\ClassRoom\ClassRoomServices;

class ClassRoomController extends Controller
{
    protected ClassRoomServices $classRoomServices;

    public function __construct(ClassRoomServices $classRoomServices)
    {
        $this->classRoomServices = $classRoomServices;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return response()->json(ClassRoomResource::collection($this->classRoomServices->getAllClassRooms()));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreClassRoomRequest $request)
    {

        return response()->json(new ClassRoomResource($this->classRoomServices->storeClassRoom($request)));
    }

    /**
     * Display the specified resource.
     */
    public function show(ClassRoom $classRoom)
    {
        return response()->json(new ClassRoomResource($this->classRoomServices->getClassRoom($classRoom->id)));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ClassRoom $classRoom)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateClassRoomRequest $request, ClassRoom $classRoom)
    {
        return response()->json(new ClassRoomResource($this->classRoomServices->updateClassRoom($request, $classRoom->id)));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ClassRoom $classRoom)
    {
        $this->classRoomServices->deleteClassRoom($classRoom->id);
        return response()->json(null, 204);
    }
}
