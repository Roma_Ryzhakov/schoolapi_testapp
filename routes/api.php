<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\StudentController;
use \App\Http\Controllers\ClassRoomController;
use \App\Http\Controllers\LectureController;
use \App\Http\Controllers\CurriculumController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::apiResource('students', StudentController::class);
Route::apiResource('classRoom', ClassRoomController::class);
Route::apiResource('lecture', LectureController::class);

Route::get('classRoom/{classRoom}/curriculum', [CurriculumController::class, 'getCurriculum']);

Route::post('classRoom/{classRoom}/curriculum', [CurriculumController::class, 'updateCurriculum']);
